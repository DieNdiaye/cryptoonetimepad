package org.dador;



/*NDIAYE Die Aissatou
 * HAMMADI Nedjma
 * RAMAN Shakul 
 * */
/**
 *
 */
public class MultiTimePad {

    /**
     * Main function. Loads cryptogram and displays decryption
     *
     * @param args No arguments are processed
     */
    public static void main(final String[] args) {
        String index = "000102030405060708090A0B0C0D0E0F101112131415161718191A1B1C1D1E";

        String msg0 = "ce38a99f9c89fc89e8c5c14190f7fe3f0de5c388e3670420c57db02e66ee51";
        String msg1 = "d43fb89f9092ebdbeecad10494bbf6220deed5dce36d0620c270b23223d351";
        String msg2 = "d370add2df8ce29ae3c5dc0f87bbfe715ee9d3daf27c546ddf6ba0356cf451";
        String msg3 = "d235ecd68cdcfa93e88bda0f8ce2bf2148fec3c7f928006f966ca12970ee51";
        String msg4 = "cd38a9d1df8fe694f8c7d14197febf3c48e9c488e3675464d938a7346ae940";
        String msg5 = "d370b8d79692e5dbf9c3d018c0e8f73e58e0d488f167186cd96ff3346af751";
        String msg6 = "ce38a5ccdf95fddbfddec70492bbeb394ce290dcff690020d976b67c6ae951";
        String[] messages = new String[]{msg0, msg1, msg2, msg3, msg4, msg5, msg6};
        byte[] key;
        byte[][] byteArrayMsg;
        int nbMsg = messages.length;
        byte[] tmpByteMsg;
        int i;
        byteArrayMsg = new byte[nbMsg][];

        String displayIndex = HexConverters.toPrintableHexFromByteArray(HexConverters.toByteArrayFromHex(index));
        System.out.println("Original Cryptograms :");
        System.out.println(displayIndex);
        
        // Transforme les messages sous un format "tableau d'octets" pour pouvoir les manipuler
        for (i = 0; i < nbMsg; i++) {
            tmpByteMsg = HexConverters.toByteArrayFromHex(messages[i]);
            byteArrayMsg[i] = tmpByteMsg;
            System.out.println(HexConverters.toPrintableHexFromByteArray(byteArrayMsg[i]));
        }

        System.out.println();
        
        
        key = new byte[msg1.length() / 2];
        // Fill in the key ...
        //Convert Hex msg to byte
        String[] c0xorciPrint=new String[7];
        String CopyCxorC;
        String [] []tabCxorC = new String[7][32];
        		
        System.out.println("            "+displayIndex);
        
        /***************************************************/
        for( i=1; i<7;i++) {
        	byte []c0xorci = HexConverters.xorArray(byteArrayMsg[0],byteArrayMsg[i]);
        	 c0xorciPrint[i] = HexConverters.toPrintableHexFromByteArray(c0xorci);
        	 
        	 CopyCxorC= c0xorciPrint[i];
        	 tabCxorC[i]=CopyCxorC.split(" ",32);
        	
            System.out.println("c0 xor c"+i +" = "+ c0xorciPrint[i]);
        }
        
        /**********2ieme etape*************************/
        String []Cmsg= new String[7];
        String CopyCmsg;
        String [] []tabCMsg = new String[7][32];
        
        //Decoupage msg crypt�
         for( i=0; i<7;i++) {
                	
         	 Cmsg[i]= HexConverters.toPrintableHexFromByteArray(byteArrayMsg[i]); 
         	 CopyCmsg=Cmsg[i];
         	 tabCMsg[i]=CopyCmsg.split(" ",32);
         }

        /* for( i=0; i<7;i++) {
         	for(int j=0; j<32;j++) {
         		 System.out.println("TAB Cmsg AFTER SPLIT c"+i +" = "+ tabCMsg[i][j]);
         		}
         	}*/
         
         /**************************************/
        
       /* for( i=1; i<7;i++) {
        	for(int j=0; j<32;j++) {
        		 System.out.println("TAB AFTER SPLIT c0 xor c"+i +" = "+ tabCxorC[i][j]);
        		}
        	}*/
         
         byte[] keyMessage=new byte[1];
    
        for( i=1; i<7;i++) {
        	for(int j=0; j<32;j++) {
        		if(tabCxorC[i][j].startsWith("4") ||tabCxorC[i][j].startsWith("5")||tabCxorC[i][j].startsWith("6")|| tabCxorC[i][j].startsWith("7") )
        		{
        			//k=Ci xor 20
        		 	keyMessage = returnKey_of_msg("20",tabCMsg[i-1][j]); 
        			System.out.println("Value after C0 xor C"+i+" : "+tabCxorC[i][j]+ " coordinate line: "+ i+ " column : "+j);
        		    System.out.println("value in Original Cryptogram  :"+ tabCMsg[i-1][j]+ " coordinate line: "+ i+ " column : "+j);
        		    System.out.println("Key of msg with position :"+HexConverters.toPrintableHexFromByteArray(keyMessage));
        		    System.out.println();
        		}        		 
        	}
        }
        
      
      
       //Pour v�rifier et afficher un � un les cl�s        
      /* byte[] keym2Col1 = returnKey_of_msg("20","70");    
       System.out.println("Key of msg with position :"+HexConverters.toPrintableHexFromByteArray(keym2Col1));
		*/


        System.out.println("Key :");
        System.out.println(displayIndex);
        System.out.println(HexConverters.toPrintableHexFromByteArray(key));

        // Affichage des messages décodés
        System.out.println();
        System.out.println("Decoded messages :");
        for (i = 0; i < nbMsg; i++) {
            tmpByteMsg = HexConverters.xorArray(key, byteArrayMsg[i]);
            System.out.println(HexConverters.toPrintableString(tmpByteMsg));
        }
    }

    //Methode permettant de retouner la cl� associe � un espace, on lui donne les valeurs Hexadecimal des deux bout de messages Ci 
	public static byte[] returnKey_of_msg(String space,String Value_inPosition_OfMsgWithSpace) {
		
		   byte []reslt1=HexConverters.toByteArrayFromHex(space);
		   byte []reslt2=HexConverters.toByteArrayFromHex(Value_inPosition_OfMsgWithSpace);
		   
		   byte []keyMsg =HexConverters.xorArray(reslt1,reslt2);
		return keyMsg;
	}
}
